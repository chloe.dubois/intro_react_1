import React, { useState, useEffect } from 'react';

function Countries() {
    const [error, setError] = useState(null);
    const [isLoaded, setIsLoaded] = useState(false);
    const [countries, setCountries] = useState([]);
    const [selectedItems, setSelectedItems] = useState([]);
    const [email, setEmail] = useState('');
    const [emailValid, setEmailValid] = useState(false);
    const [formValid, setFormValid] = useState(false);

    const add = item => {
      if (!selectedItems.find(i => i.alpha2Code === item.alpha2Code)) {
        const resp = selectedItems.concat(item);
        setSelectedItems(resp)
        console.log(item)
      }
    };

    const remove = item => {
      const resp = selectedItems.filter(function( obj ) {
        return obj.alpha2Code !== item.alpha2Code;
      });
      setSelectedItems(resp)
    };

    const handleEmail = (event) => {
      setEmail(event.target.value);
      //eslint-disable-next-line
      const regexEmail = /(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;
      if (email.match(regexEmail)) {
        setEmailValid(true)
      } else {
        setEmailValid(false)
      }
    };

    const handleSubmit = () => {
      alert('An email was send to ' + email + ' with this selection: ' + selectedItems.map(country => country.name))
    }

    const isSelected = item => {
      return selectedItems.some(i => i.alpha2Code === item.alpha2Code)
    };

    // check form validity
    useEffect(() => {
        if (emailValid && selectedItems.length > 0) {
          setFormValid(true)
        } else {
          setFormValid(false)
        }
    }, [emailValid, selectedItems])

    useEffect(() => {
      fetch("https://restcountries.com/v2/all")
        .then(res => res.json())
        .then(
          (result) => {
            setIsLoaded(true);
              const items = result.reduce(function (a, b) {
                  if (!a[b['region']]) {
                    a[b['region']] = []
                  }
                  a[b['region']].push(b)
                  return a
                }, {})
            setCountries(items);
          },
          (error) => {
            setIsLoaded(true);
            setError(error);
          }
        )
    }, [])
  
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
          <div className="mainDiv">
            <div className="selectionDiv">
              <h2>Selected Countries</h2>
              { selectedItems.length < 1 &&
                <p className="message">Please select at least one country.</p>
              }
              <ul>
                { selectedItems.map(item =>
                  <button onClick={() => remove(item)} className="removeBtn" key={item.alpha2Code}>
                    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="x-icon" viewBox="0 0 16 16">
                      <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"/>
                    </svg>
                    {item.name}
                  </button>
                )}
              </ul>
              <input
                refs="email"
                type="email"
                placeholder="Enter a valid Email"
                onChange={handleEmail}
                value={email}
              />
              <button onClick={handleSubmit} className={formValid? "submitBtn valid" : "submitBtn invalid"} disabled={!formValid}>Submit Selection</button>
            </div> 
            <div className="countriesDiv">
              { Object.keys(countries).map(entry =>
                <div key={entry}>
                  <h2>{entry}</h2>
                  <div className="btnDiv">
                    {countries[entry].map(item => (
                      <button onClick={() => add(item)} className={isSelected(item)? "addBtn selected" : "addBtn"} key={item.alpha2Code}>
                        <div className="flag"><img src={item.flags.svg} alt="flag" /></div>
                        <div>{item.name}</div>
                      </button>
                    ))}
                  </div>
                </div>
              )}
            </div> 
        </div>      
      );
    }
  }

export default Countries;

